var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var initSize = 0.4
var steps = 64

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(128 + 128 * sin(frameCount * 0.025))
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  push()
  translate(windowWidth * 0.5, windowHeight * 0.5)
  rotate(sin(frameCount * 0.01))
  for (var i = 0; i < steps; i++) {
    noStroke()
    fill(128 * (1 - i / steps) + 128 * (1 - i / steps) * sin(frameCount * 0.025 + i * 0.05))
    drawSplash(7, initSize * 0.9 - i * (initSize / (steps + 10)), initSize * sqrt(4 / 3) - i * (initSize * sqrt(4 / 3) / (steps + 10)))
  }
  pop()
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function drawSplash(numOfEdges, maxSize, minSize, randomness) {
  beginShape()
  for (var i = 0; i < numOfEdges * 2; i++) {
    var direction = Math.PI * 2 * (i / numOfEdges)
    var distance
    if (i % 2 === 0) {
      var rand = Math.random()
      distance = maxSize
    } else {
      distance = minSize
    }
    var posX = distance * sin(direction + frameCount * 0.01) * boardSize
    var posY = distance * cos(direction + frameCount * 0.01) * boardSize
    vertex(posX, posY)
  }
  endShape(CLOSE)
}
